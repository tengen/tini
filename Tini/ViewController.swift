//
//  ViewController.swift
//  Tini
//
//  Created by Daniel on 11/10/2018.
//  Copyright © 2018 TapTap. All rights reserved.
//

import UIKit
import ApiAI
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var response: UILabel!
    @IBOutlet weak var message: UITextField!
    
    @IBOutlet weak var messageConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        unregisterKeyboardNotifications()
        registerKeyboardNotifications()
        message.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func sendMessage(_ sender: Any) {
        guard let text = message.text, text != "" else { return }
        let request = ApiAI.shared().textRequest()
        request?.query = text
        request?.setMappedCompletionBlockSuccess({ (request, response) in
            let response = response as! AIResponse
            print(response.result.action)
            print(response.result.parameters)
            print(response.result.fulfillment.messages)
            if let textResponse = response.result.fulfillment.messages.first?["speech"] as? String {
                self.speechAndText(text: textResponse, completion: {
                    print("speech completed")
                    print(response.result.action)
                    if response.result.action == "output.mothers_around" {
                        self.displayMap()
                    }
                })
            }
        }, failure: { (request, error) in
            print(error!)
        })

        ApiAI.shared().enqueue(request)
        message.text = ""
    }

    let speechSynthesizer = AVSpeechSynthesizer()

    func speechAndText(text: String, completion: @escaping () -> Swift.Void) {
        let speechUtterance = AVSpeechUtterance(string: text)
        speechSynthesizer.speak(speechUtterance)
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.response.text = text
        }, completion: nil)
        completion()
    }

    func displayMap() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let map = storyboard.instantiateViewController(withIdentifier: "map")
        self.present(map, animated: true, completion: nil)
    }

    // Keyboard notifications

    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }

    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        animateMessageWithKeyboard(notification: notification, up: true)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        animateMessageWithKeyboard(notification: notification, up: false)
    }

    func animateMessageWithKeyboard(notification: NSNotification, up: Bool) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo.object(forKey:UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        let curve = userInfo.object(forKey:UIKeyboardAnimationCurveUserInfoKey) as! UInt

        messageConstraint.constant = up ? keyboardSize.height : 0
        self.view.setNeedsLayout()
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: UIViewAnimationOptions(rawValue: curve),
                       animations: {
                        self.view.layoutIfNeeded()
        },
                       completion: nil)
    }

}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage(textField)
        return true
    }
}

